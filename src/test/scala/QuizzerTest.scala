import org.scalatest.{Matchers, BeforeAndAfter}

class QuizzerTest extends org.scalatest.FlatSpec with BeforeAndAfter with Matchers{

  var evaluator: Evaluator = _
  var scores: Scores = _

  before {
    val fileQuizz:String ="files/Quizz.json"
    val fileAssessment:String ="files/Assessment.json"
    val fileScores:String ="files/Scores.json"

    val deserialization = new Deserialization()
    val quizz = new Quizz(deserialization.parseQuizzJson(deserialization.readFile(fileQuizz)))
    val assessment = new Assessment(deserialization.parseAssessmentJson(deserialization.readFile(fileAssessment)))
    scores = new Scores(deserialization.parseScoresJson(deserialization.readFile(fileScores)))
    evaluator = new Evaluator(quizz.getQuestions, assessment.getAssessment)
  }

  "The evaluator" should "calculate scores" in {

    evaluator.evaluate() should be (scores)
  }
}
