import java.io.{File, FileWriter}

import scala.collection.mutable.HashMap

class Scores(scores: HashMap[Int, Score]) {
  def getScores: HashMap[Int, Score] = {
    return scores
  }

  def writeScores(): Unit = {
    var counter: Int = 0
    var json: String = "{\"scores\":[";
    for (studentId <- scores.keySet) {
      json += "{\"studentId\": " + studentId + ", \"value\": " + scores.get(studentId).get.getValue + "}";
      if (counter != scores.size - 1) {
        json += ", ";
      }
      counter += 1;
    }
    json += "]}";
    val newTextFile = new File("files/Scores.json");
    val fileWriter = new FileWriter(newTextFile);
    fileWriter.write(json);
    fileWriter.close();
  }

}