class Score(studentId: Int, value: Double) {
  def getStudentId: Int = {
    return studentId
  }

  def getValue: Double = {
    return value
  }
}