class Truefalse(id: Int, questionText: String, correct: Boolean, valueOK: Double, valueFailed: Double, feedback: String) extends Question(id: Int, questionText: String) {
  override def getStudentAnswerScore(answer: Answer): Double = {
    if (answer.getValue == correct.toString) {
      return valueOK
    }
    else {
      return valueFailed
    }
  }
}