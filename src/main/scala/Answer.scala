class Answer(question: Int, value: String) {
  def getQuestion: Int = {
    return question
  }

  def getValue: String = {
    return value
  }
}
