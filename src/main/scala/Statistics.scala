import java.io.{File, FileWriter}

import scala.collection.mutable.HashMap

class Statistics(statistics: HashMap[Int, Int]) {
  def writeStatistics(fileCounter: Int) {
    var counter: Int = 0
    var json: String = "{\"scores\":[";
    for (questionId <- statistics.keySet) {
      json += "{\"questionId\": " + questionId + ", \"value\": " + statistics.get(questionId).get + "}";
      if (counter != statistics.size - 1) {
        json += ", ";
      }
      counter += 1;
    }
    json += "]}";

    if (fileCounter == 0) {
      val newTextFile = new File("files/Statistics.json");
      val fileWriter = new FileWriter(newTextFile);
      fileWriter.write(json);
      fileWriter.close();
    } else {
      val newTextFile = new File("files/Statistics" + fileCounter + ".json");
      val fileWriter = new FileWriter(newTextFile);
      fileWriter.write(json);
      fileWriter.close();
    }
  }
}