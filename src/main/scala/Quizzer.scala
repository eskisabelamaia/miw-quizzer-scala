

object Quizzer {
  def main(args: Array[String]): Unit = {

    if (args.length < 1 || args.length > 2) {
      System.out.println("Invalid command")
    } else if (args.length == 1) {
      var fileCounter: Int = 1
      val deserialization = new Deserialization()
      val manifest = deserialization.parseManifestJson(deserialization.readFile(args(0)))
      for (counter <- manifest.keySet) {
        val quizz = new Quizz(deserialization.parseQuizzJson(deserialization.readFileFromURL(manifest.get(counter).get(2))))
        val assessment = new Assessment(deserialization.parseAssessmentJson(deserialization.readFileFromURL(manifest.get(counter).get(1))))
        val scores = new Scores(deserialization.parseScoresJson(deserialization.readFileFromURL(manifest.get(counter).get(0))))
        val evaluator = new Evaluator(quizz.getQuestions, assessment.getAssessment)
        val calculatedScores = new Scores(evaluator.evaluate())
        evaluator.compareScores(scores.getScores, calculatedScores.getScores)
        var statistics = new Statistics(evaluator.getAnswersStatistics())
        statistics.writeStatistics(fileCounter)
        System.out.println("Calculated statistics have been written in the path files/Statistics" + fileCounter + ".json");
        fileCounter += 1
      }
    } else if (args.length == 2) {
      val deserialization = new Deserialization()
      val quizz = new Quizz(deserialization.parseQuizzJson(deserialization.readFile(args(0))))
      val assessment = new Assessment(deserialization.parseAssessmentJson(deserialization.readFile(args(1))))
      val evaluator = new Evaluator(quizz.getQuestions, assessment.getAssessment)
      val scores = new Scores(evaluator.evaluate())
      scores.writeScores()
      System.out.println("Calculated scores have been written in the path files/scores.json");
      val statistics = new Statistics(evaluator.getAnswersStatistics())
      statistics.writeStatistics(0)
      System.out.println("Calculated statistics have been written in the path files/statistics.json");
    }
  }
}
