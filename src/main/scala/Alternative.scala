class Alternative(code: Int, text: String, value: Double) {
  def getCode: Int = {
    return code
  }

  def getText: String = {
    return text
  }

  def getValue: Double = {
    return value
  }
}
