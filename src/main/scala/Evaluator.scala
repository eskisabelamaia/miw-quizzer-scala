import scala.collection.mutable.HashMap

class Evaluator(quizz: HashMap[Int, Question], assessment: HashMap[Int, List[Answer]]) {

  def evaluate(): HashMap[Int, Score] = {
    val scores: HashMap[Int, Score] = new HashMap[Int, Score]
    for (studentId <- assessment.keySet) {
      if (assessment.contains(studentId)) {
        val grade: Double = scoreCalculation(studentId, assessment.get(studentId).get)
        val score: Score = new Score(studentId, grade)
        scores.put(studentId, score)
      }
    }
    return scores
  }

  def scoreCalculation(studentId: Int, answers: List[Answer]): Double = {
    var score: Double = 0.0
    var i: Int = 0
    for (answer <- answers) {
      if (quizz.contains(answer.getQuestion)) {
        score += quizz.get(answer.getQuestion).get.getStudentAnswerScore(answer)
      }
      i += 1
    }
    return score
  }

  def compareScores(scores: HashMap[Int, Score], calculatedScores: HashMap[Int, Score]): Unit = {
    var counter: Int = 0
    for (studentId <- scores.keySet) {
      if (calculatedScores.contains(studentId)) {
        if (scores.get(studentId).get.getValue == calculatedScores.get(studentId).get.getValue) {
          counter += 1
        }
      }
    }
    if (counter == scores.size) {
      System.out.println("Calculated scores are equals");
    } else {
      System.out.println("Calculated scores are not equals");
    }
  }

  def getAnswersStatistics(): HashMap[Int, Int] = {
    val statistics: HashMap[Int, Int] = new HashMap[Int, Int]
    for (studentId <- assessment.keySet) {
      if (assessment.contains(studentId)) {
        for (answer <- assessment.get(studentId).get) {
          if (quizz.contains(answer.getQuestion)) {
            if (quizz.get(answer.getQuestion).get.getStudentAnswerScore(answer) > 0) {
              if (statistics.contains(answer.getQuestion)) {
                statistics.put(answer.getQuestion, statistics.get(answer.getQuestion).get + 1)
              } else {
                statistics.put(answer.getQuestion, 1)
              }
            }
          }
        }
      }
    }
    return statistics
  }
}