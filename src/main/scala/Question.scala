abstract class Question(id: Int, questionText: String) {

  def getStudentAnswerScore(answer: Answer): Double
}