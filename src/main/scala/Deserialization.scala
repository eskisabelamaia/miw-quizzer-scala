import play.api.libs.json.{JsObject, Json}

import scala.collection.mutable._
import scala.io.Source

class Deserialization() {

  def readFileFromURL(url: String): String = {
    val file = io.Source.fromURL(url).mkString
    file
  }

  def readFile(file: String): String = {
    val strJSON = Source.fromFile(file).getLines.mkString
    strJSON
  }

  def parseQuizzJson(strJSON: String): HashMap[Int, Question] = {
    var questions = new HashMap[Int, Question]()
    var strQuizz = Json.parse(strJSON)
    for (question <- (strQuizz \ "questions").as[List[JsObject]]) {
      val typeQ = (question \ "type").as[String]
      val q = typeQ match {
        case "multichoice" => parseMultichoice(question)
        case "truefalse" => parseTruefalse(question)
      }
      questions.put((question \ "id").as[Int], q)
    }
    questions
  }

  def parseAssessmentJson(strJSON: String): HashMap[Int, List[Answer]] = {
    val items = new HashMap[Int, List[Answer]]()
    val strAssessment = Json.parse(strJSON)
    for (item <- (strAssessment \ "items").as[List[JsObject]]) {
      val studentId = (item \ "studentId").as[Int]
      var answer = List[Answer]()

      for (a <- (item \ "answers").as[List[JsObject]]) {
        answer ::= new Answer((a \ "question").as[Int], (a \ "value").toString)
      }
      items.put(studentId, answer)
    }
    items
  }

  def parseScoresJson(strJSON: String): HashMap[Int, Score] = {
    val scores = new HashMap[Int, Score]()
    val strScores = Json.parse(strJSON)
    for (score <- (strScores \ "scores").as[List[JsObject]]) {
      val studentId = (score \ "studentId").as[Int]
      scores.put(studentId, new Score(studentId, (score \ "value").as[Double]))
    }
    scores
  }

  def parseManifestJson(strJSON: String): HashMap[Int, List[String]] = {
    val files = new HashMap[Int, List[String]]()
    var urls = List[String]()
    var counter: Int = 0
    val strManifest = Json.parse(strJSON)
    for (value <- (strManifest \ "tests").as[List[JsObject]]) {
      val urlQuizz = (value \ "quizz").as[String]
      val urlAssessment = (value \ "assessment").as[String]
      val urlScores = (value \ "scores").as[String]
      urls ::= urlQuizz
      urls ::= urlAssessment
      urls ::= urlScores
      files.put(counter, urls)
      counter += 1
    }
    files
  }

  def parseMultichoice(strQuizz: JsObject): Question = {
    var alternatives = new HashMap[Int, List[Alternative]]()
    val id = (strQuizz \ "id").as[Int]
    val questionText = (strQuizz \ "questionText").as[String]
    var alternative = List[Alternative]()
    for (a <- (strQuizz \ "alternatives").as[List[JsObject]]) {
      alternative ::= new Alternative((a \ "code").as[Int], (a \ "text").as[String], (a \ "value").as[Double])
    }
    alternatives.put(id, alternative)
    val q = new Multichoice(id, questionText, alternatives)
    q
  }

  def parseTruefalse(strQuizz: JsObject): Truefalse = {
    val id = (strQuizz \ "id").as[Int]
    val questionText = (strQuizz \ "questionText").as[String]
    val correct = (strQuizz \ "correct").as[Boolean]
    val valueOK = (strQuizz \ "valueOK").as[Double]
    val valueFailed = (strQuizz \ "valueFailed").as[Double]
    val feedback = (strQuizz \ "feedback").as[String]
    val q = new Truefalse(id, questionText, correct, valueOK, valueFailed, feedback)
    q
  }


}