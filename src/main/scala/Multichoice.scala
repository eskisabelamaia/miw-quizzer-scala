import scala.collection.mutable.HashMap

class Multichoice(id: Int, questionText: String, alternatives: HashMap[Int, List[Alternative]]) extends Question(id: Int, questionText: String) {
  override def getStudentAnswerScore(answer: Answer): Double = {
    var score: Double = 0.0
    val alternativeCode = answer.getValue

    val alternative = alternatives.get(answer.getQuestion);
    for (a <- alternative.get) {
      if (a.getCode == alternativeCode.toInt) {
        score = a.getValue
      }
    }
    score
  }
}